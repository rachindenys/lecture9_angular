﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Lecture5_Common.DTO;
using Lecture5_Common.DTO.Task;

namespace Lecture5_ConsoleApp.Services
{
    class TasksHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();


        public TasksHttpService()
        {
            _httpClient.BaseAddress = new Uri("http://localhost:5000/api/Tasks/");
        }
        public async Task<List<TaskDTO>> GetAllTasks()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<TaskDTO>>(content);
        }

        public async Task<List<TaskDTO>> GetAllUncompletedTasks()
        {
            var content = await _httpClient.GetStringAsync("uncompleted");
            return JsonConvert.DeserializeObject<List<TaskDTO>>(content);
        }

        public async Task<TaskDTO> GetTaskById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<TaskDTO>(content);
        }

        public async Task<HttpResponseMessage> DeleteTask(int id)
        {
            return await _httpClient.DeleteAsync($"{id}");
        }

        public async Task<TaskDTO> UpdateTask(TaskDTO item)
        {
            var content = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8, "application/json");
            var result = await _httpClient.PutAsync($"{item.Id}", content);
            return JsonConvert.DeserializeObject<TaskDTO>(await result.Content.ReadAsStringAsync());
        }
    }
}
