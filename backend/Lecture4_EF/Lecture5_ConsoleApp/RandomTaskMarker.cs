﻿using Lecture5_ConsoleApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_ConsoleApp
{
    public static class RandomTaskMarker
    {
        private static readonly TasksHttpService _tasksService = new TasksHttpService();
        public static async Task<int> MarkRandomTaskWithDelay(int delay)
        {
            var tcs = new TaskCompletionSource<int>();
            var timer = new System.Timers.Timer(delay);
            timer.Elapsed += async (o, e) =>
            {
                try
                {
                    var tasks = await _tasksService.GetAllUncompletedTasks();
                    if (tasks.Count == 0)
                    {
                        Console.WriteLine("All tasks marked as finished");
                        tcs.SetException(new IndexOutOfRangeException());
                    }
                    var taskIndex = new Random().Next(1, tasks.Count);
                    var taskToUpdate = tasks[taskIndex];
                    taskToUpdate.State = Lecture5_Common.DTO.Task.TaskStateDTO.Finished;
                    await _tasksService.UpdateTask(taskToUpdate);
                    tcs.SetResult(taskToUpdate.Id);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            };
            timer.AutoReset = false;
            timer.Enabled = true;

            return await tcs.Task;
        }
    }
}
