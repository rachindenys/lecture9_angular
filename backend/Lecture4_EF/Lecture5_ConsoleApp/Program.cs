﻿using System;
using System.Threading.Tasks;

namespace Lecture5_ConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            while (true)
            {
                await ConsoleMenu.Start();
            }
        }
    }
}
