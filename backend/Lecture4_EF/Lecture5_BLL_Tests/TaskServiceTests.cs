﻿using AutoMapper;
using Lecture5_BLL.Services;
using Lecture5_Common.DTO.Project;
using Lecture5_Common.DTO.Task;
using Lecture5_Common.DTO.Team;
using Lecture5_Common.DTO.User;
using Lecture5_DAL;
using Lecture5_DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Lecture5_BLL_Tests
{
    public class TaskServiceTests : IDisposable
    {
        readonly AcademyDbContext context;
        readonly DbContextOptions<AcademyDbContext> options;
        readonly TaskService taskService;
        readonly IMapper mapper;

        public TaskServiceTests()
        {
            options = new DbContextOptionsBuilder<AcademyDbContext>()
                            .UseInMemoryDatabase("TaskServiceTestDB")
                            .Options;
            context = new AcademyDbContext(options);
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Project, ProjectDTO>().ReverseMap();
                cfg.CreateMap<Task, TaskDTO>().ReverseMap();
                cfg.CreateMap<User, UserDTO>().ReverseMap();
                cfg.CreateMap<Team, TeamDTO>().ReverseMap();
                cfg.CreateMap<TaskState, TaskStateDTO>().ReverseMap();
            });
            mapper = config.CreateMapper();
            taskService = new TaskService(context, mapper);
        }

        public void Dispose()
        {
            context.Database.EnsureDeleted();
        }

        [Fact]
        public async void Update_WhenStateChangedToFinished_ThenTaskStateUpdated()
        {
            var task = new Task { Id = 1, State = TaskState.Started, FinishedAt = null };
            var taskDto = new TaskDTO { Id = 1, State = TaskStateDTO.Finished, FinishedAt = DateTime.Now };
            context.AddRange(task);
            context.SaveChanges();
            await taskService.Update(taskDto);
            var updatedTask = context.Tasks.Find(1);
            Assert.True(updatedTask.State == TaskState.Finished && updatedTask.FinishedAt.Value.Day == DateTime.Now.Day);
        }


    }
}
