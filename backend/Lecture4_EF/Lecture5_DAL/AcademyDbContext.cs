﻿using Lecture5_DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture5_DAL
{
    public class AcademyDbContext : DbContext
    {
        public AcademyDbContext(DbContextOptions<AcademyDbContext> options)
            : base(options) 
            { }

        public AcademyDbContext()
        
        { }

        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    AuthorId = 4,
                    TeamId = 3,
                    Name = "open architecture Outdoors, Grocery & Baby Dynamic",
                    Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
                    Deadline = new DateTime(2021, 8, 3),
                    CreatedAt = new DateTime(2019, 7, 17)
                }, 
                new Project
                {
                    Id = 2,
                    AuthorId = 3,
                    TeamId = 2,
                    Name = "backing up Handcrafted Fresh Shoes challenge",
                    Description = "Et doloribus et temporibus.",
                    Deadline = new DateTime(2021, 9, 12),
                    CreatedAt = new DateTime(2020, 8, 25)
                },
                new Project
                {
                    Id = 3,
                    AuthorId = 2, 
                    TeamId = 1,
                    Name = "Village",
                    Description = "Non voluptatem voluptas libero.",
                    Deadline = new DateTime(2021, 7, 24),
                    CreatedAt = new DateTime(2021, 1, 30)
                },
                new Project
                {
                    Id = 4,
                    AuthorId = 1,
                    TeamId = 4,
                    Name = "Dam",
                    Description = "Quia et tempora hic pariatur voluptatem doloribus sunt.",
                    Deadline = new DateTime(2021, 7, 21),
                    CreatedAt = new DateTime(2020, 3, 15)
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    ProjectId = 2,
                    PerformerId = 4,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Cancelled,
                    CreatedAt = new DateTime(2017, 10, 31),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 2,
                    ProjectId = 3,
                    PerformerId = 3,
                    Name = "real-time",
                    Description = "Quo sint aut et ea voluptatem omnis ut.",
                    State = TaskState.Cancelled,
                    CreatedAt = new DateTime(2020, 5, 15),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 3,
                    ProjectId = 4,
                    PerformerId = 2,
                    Name = "product Direct utilize",
                    Description = "Eum a eum.",
                    State = TaskState.Created,
                    CreatedAt = new DateTime(2019, 2, 15),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 4,
                    ProjectId = 1,
                    PerformerId = 1,
                    Name = "bypass",
                    Description = "Sint voluptatem quas.",
                    State = TaskState.Finished,
                    CreatedAt = new DateTime(2017, 8, 16),
                    FinishedAt = new DateTime(2020, 9, 9)
                },
                new Task
                {
                    Id = 5,
                    ProjectId = 2,
                    PerformerId = 4,
                    Name = "withdrawal contextually-based",
                    Description = "Delectus quibusdam id quia iure neque maiores molestias sed aut.",
                    State = TaskState.Started,
                    CreatedAt = new DateTime(2018, 10, 19),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 6,
                    ProjectId = 3,
                    PerformerId = 3,
                    Name = "mobile Organized",
                    Description = "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.",
                    State = TaskState.Started,
                    CreatedAt = new DateTime(2018, 6, 15),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 7,
                    ProjectId = 4,
                    PerformerId = 2,
                    Name = "Auto Loan Account Cambridgeshire",
                    Description = "Aut tenetur voluptas quasi esse.",
                    State = TaskState.Finished,
                    CreatedAt = new DateTime(2019, 2, 5),
                    FinishedAt = new DateTime(2020, 12, 4)
                },
                new Task
                {
                    Id = 8,
                    ProjectId = 1,
                    PerformerId = 1,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = TaskState.Cancelled,
                    CreatedAt = new DateTime(2017, 10, 31),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 9,
                    ProjectId = 2,
                    PerformerId = 4,
                    Name = "Intelligent Granite Mouse",
                    Description = "Aliquam laboriosam consequatur qui.",
                    State = TaskState.Created,
                    CreatedAt = new DateTime(2020, 3, 23),
                    FinishedAt = null
                },
                new Task
                {
                    Id = 10,
                    ProjectId = 3,
                    PerformerId = 3,
                    Name = "online",
                    Description = "Optio eum aut sunt cum nam.",
                    State = TaskState.Started,
                    CreatedAt = new DateTime(2019, 8, 27),
                    FinishedAt = null
                },
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    TeamId = 2,
                    FirstName = "Vivian",
                    LastName = "Mertz",
                    Email = "Vivian99@yahoo.com",
                    RegisteredAt = new DateTime(2018, 10, 28),
                    BirthDay = new DateTime(1953, 12, 23)
                },
                new User
                {
                    Id = 2,
                    TeamId = 4,
                    FirstName = "Theresa",
                    LastName = "Gottlieb",
                    Email = "Theresa_Gottlieb66@yahoo.com",
                    RegisteredAt = new DateTime(2019, 12, 04),
                    BirthDay = new DateTime(2014, 9, 27)
                },
                new User
                {
                    Id = 3,
                    FirstName = "Brandy",
                    LastName = "Witting",
                    Email = "Brandy.Witting@gmail.com",
                    RegisteredAt = new DateTime(2019, 1, 10),
                    BirthDay = new DateTime(2007, 1, 1)
                },
                new User
                {
                    Id = 4,
                    FirstName = "Theresa",
                    LastName = "Ebert",
                    Email = "Theresa82@hotmail.com",
                    RegisteredAt = new DateTime(2017, 5, 14),
                    BirthDay = new DateTime(2015, 11, 22)
                },
            };

            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    Name = "Denesik - Greenfelder",
                    CreatedAt = new DateTime(2019, 8, 25)
                },
                new Team
                {
                    Id = 2,
                    Name = "Durgan Group",
                    CreatedAt = new DateTime(2017, 3, 31)
                },
                new Team
                {
                    Id = 3,
                    Name = "Kassulke LLC",
                    CreatedAt = new DateTime(2019, 2, 21)
                },
                new Team
                {
                    Id = 4,
                    Name = "Harris LLC",
                    CreatedAt = new DateTime(2018, 8, 28)
                },
                new Team
                {
                    Id = 5,
                    Name = "Mitchell Inc",
                    CreatedAt = new DateTime(2019, 4, 3)
                },
                new Team
                {
                    Id = 6,
                    Name = "Smitham Group",
                    CreatedAt = new DateTime(2016, 10, 5)
                },
                new Team
                {
                    Id = 7,
                    Name = "Kutch - Roberts",
                    CreatedAt = new DateTime(2016, 10, 31)
                },
                new Team
                {
                    Id = 8,
                    Name = "Parisian Group",
                    CreatedAt = new DateTime(2016, 7, 17)
                },
                new Team
                {
                    Id = 9,
                    Name = "Schiller Group",
                    CreatedAt = new DateTime(2020, 10, 5)
                },
                new Team
                {
                    Id = 10,
                    Name = "Littel, Turcotte and Mulle",
                    CreatedAt = new DateTime(2018, 10, 19)
                },
            };
            modelBuilder.Entity<Project>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<Task>()
                .Property(t => t.Id)
                .ValueGeneratedOnAdd();
           modelBuilder.Entity<Team>()
                .Property(t => t.Id)
                .ValueGeneratedOnAdd();
           modelBuilder.Entity<User>()
                .Property(u => u.Id)
                .ValueGeneratedOnAdd();


            modelBuilder.Entity<User>()
                .HasMany(u => u.Projects)
                .WithOne(p => p.Author)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>()
                .HasMany(c => c.Tasks)
                .WithOne(t => t.Performer)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Task>()
                .HasOne(t => t.Performer)
                .WithMany(u => u.Tasks)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>()
                .HasOne(u => u.Team)
                .WithMany(t => t.Users);
            

            modelBuilder
                .Entity<Task>()
                .Property(e => e.State)
                .HasConversion<int>();

            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
        }
    }
}
