﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Lecture5_DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TeamId = table.Column<int>(type: "int", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegisteredAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BirthDay = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    TeamId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Deadline = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Projects_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    PerformerId = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    State = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tasks_Users_PerformerId",
                        column: x => x.PerformerId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 8, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Denesik - Greenfelder" },
                    { 2, new DateTime(2017, 3, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Durgan Group" },
                    { 3, new DateTime(2019, 2, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kassulke LLC" },
                    { 4, new DateTime(2018, 8, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Harris LLC" },
                    { 5, new DateTime(2019, 4, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mitchell Inc" },
                    { 6, new DateTime(2016, 10, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Smitham Group" },
                    { 7, new DateTime(2016, 10, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kutch - Roberts" },
                    { 8, new DateTime(2016, 7, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Parisian Group" },
                    { 9, new DateTime(2020, 10, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Schiller Group" },
                    { 10, new DateTime(2018, 10, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Littel, Turcotte and Mulle" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 3, new DateTime(2007, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Brandy.Witting@gmail.com", "Brandy", "Witting", new DateTime(2019, 1, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 4, new DateTime(2015, 11, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Theresa82@hotmail.com", "Theresa", "Ebert", new DateTime(2017, 5, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), null }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 2, 3, new DateTime(2020, 8, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 9, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Et doloribus et temporibus.", "backing up Handcrafted Fresh Shoes challenge", 2 },
                    { 1, 4, new DateTime(2019, 7, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 8, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.", "open architecture Outdoors, Grocery & Baby Dynamic", 3 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(1953, 12, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Vivian99@yahoo.com", "Vivian", "Mertz", new DateTime(2018, 10, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 },
                    { 2, new DateTime(2014, 9, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Theresa_Gottlieb66@yahoo.com", "Theresa", "Gottlieb", new DateTime(2019, 12, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), 4 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 4, 1, new DateTime(2020, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 7, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Quia et tempora hic pariatur voluptatem doloribus sunt.", "Dam", 4 },
                    { 3, 2, new DateTime(2021, 1, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 7, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "Non voluptatem voluptas libero.", "Village", 1 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 1, new DateTime(2017, 10, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Eveniet nihil asperiores esse minima.", null, "index", 4, 2, 3 },
                    { 5, new DateTime(2018, 10, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Delectus quibusdam id quia iure neque maiores molestias sed aut.", null, "withdrawal contextually-based", 4, 2, 1 },
                    { 9, new DateTime(2020, 3, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Aliquam laboriosam consequatur qui.", null, "Intelligent Granite Mouse", 4, 2, 0 },
                    { 4, new DateTime(2017, 8, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sint voluptatem quas.", new DateTime(2020, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "bypass", 1, 1, 2 },
                    { 8, new DateTime(2017, 10, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Eveniet nihil asperiores esse minima.", null, "index", 1, 1, 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 3, new DateTime(2019, 2, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Eum a eum.", null, "product Direct utilize", 2, 4, 0 },
                    { 7, new DateTime(2019, 2, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Aut tenetur voluptas quasi esse.", new DateTime(2020, 12, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Auto Loan Account Cambridgeshire", 2, 4, 2 },
                    { 2, new DateTime(2020, 5, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Quo sint aut et ea voluptatem omnis ut.", null, "real-time", 3, 3, 3 },
                    { 6, new DateTime(2018, 6, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.", null, "mobile Organized", 3, 3, 1 },
                    { 10, new DateTime(2019, 8, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Optio eum aut sunt cum nam.", null, "online", 3, 3, 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_AuthorId",
                table: "Projects",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_PerformerId",
                table: "Tasks",
                column: "PerformerId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
