﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Lecture5_DAL.Entities
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public List<User> Users { get; set; }
    }
}
