﻿using AutoMapper;
using Lecture5_BLL.Exceptions;
using Lecture5_BLL.Services.Abstract;
using Lecture5_Common.DTO;
using Lecture5_Common.DTO.Task;
using Lecture5_DAL;
using Lecture5_DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_BLL.Services
{
    public sealed class TaskService : BaseService<Lecture5_DAL.Entities.Task>
    {
        public TaskService(AcademyDbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<TaskDTO> Create(TaskDTO taskDTO)
        {
            var task = _mapper.Map<Lecture5_DAL.Entities.Task>(taskDTO);
            _context.Tasks.Add(task);
            await _context.SaveChangesAsync();
            return _mapper.Map<TaskDTO>(task);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var task = await _context.Tasks.FindAsync(id);
            if (task == null)
            {
                throw new NotFoundException(nameof(Lecture5_DAL.Entities.Task), id);
            }
            _context.Tasks.Remove(task);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<TaskDTO>> Get()
        {
            var tasks = await _context.Tasks.ToListAsync();
            return _mapper.Map<ICollection<TaskDTO>>(tasks);
        }

        public async Task<ICollection<TaskDTO>> GetUncompleted()
        {
            var tasks = await _context.Tasks.Where(t => t.State != TaskState.Finished).ToListAsync();
            return _mapper.Map<ICollection<TaskDTO>>(tasks);
        }

        public async Task<TaskDTO> Get(int id)
        {
            var task = await _context.Tasks.FindAsync(id);
            if (task == null)
            {
                throw new NotFoundException(nameof(Lecture5_DAL.Entities.Task), id);
            }
            return _mapper.Map<TaskDTO>(task);
        }

        public async System.Threading.Tasks.Task<TaskDTO> Update(TaskDTO taskDTO)
        {
            var task = await _context.Tasks.FindAsync(taskDTO.Id);
            if (task == null)
            {
                throw new NotFoundException(nameof(Lecture5_DAL.Entities.Task), taskDTO.Id);
            }
            task.CreatedAt = taskDTO.CreatedAt;
            task.Description = taskDTO.Description;
            task.FinishedAt = taskDTO.FinishedAt;
            task.Name = taskDTO.Name;
            task.PerformerId = taskDTO.PerformerId;
            task.ProjectId = taskDTO.ProjectId;
            task.State = _mapper.Map<TaskState>(taskDTO.State);
            //task.Performer = _mapper.Map<User>(taskDTO.Performer);
            //task.Project = _mapper.Map<Project>(taskDTO.Project);
            _context.Tasks.Update(task);
            await _context.SaveChangesAsync();
            return _mapper.Map<TaskDTO>(task);
        }
    }
}
