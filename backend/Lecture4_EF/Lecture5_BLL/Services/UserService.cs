﻿using AutoMapper;
using Lecture5_BLL.Exceptions;
using Lecture5_BLL.Services.Abstract;
using Lecture5_Common.DTO;
using Lecture5_Common.DTO.User;
using Lecture5_DAL;
using Lecture5_DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_BLL.Services
{
    public sealed class UserService : BaseService<User>
    {
        public UserService(AcademyDbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<UserDTO> Create(UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return _mapper.Map<UserDTO>(user);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), id);
            }
            var tasks = _context.Tasks.Where(t => t.PerformerId == user.Id);
            foreach (var task in tasks)
            {
                task.PerformerId = null;
                _context.Tasks.Update(task);
            }
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<UserDTO>> Get()
        {
            var users = await _context.Users.ToListAsync();
            return _mapper.Map<ICollection<UserDTO>>(users);
        }

        public async Task<UserDTO> Get(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), id);
            }
            return _mapper.Map<UserDTO>(user);
        }

        public async System.Threading.Tasks.Task Update(UserDTO userDTO)
        {
            var user = await _context.Users.FindAsync(userDTO.Id);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), userDTO.Id);
            }
            user.BirthDay = userDTO.BirthDay;
            user.Email = userDTO.Email;
            user.FirstName = userDTO.FirstName;
            user.LastName = userDTO.LastName;
            user.RegisteredAt = userDTO.RegisteredAt;
            //user.Tasks = _mapper.Map<List<Lecture5_DAL.Entities.Task>>(userDTO.Tasks);
            //user.Team = _mapper.Map<Team>(userDTO.Team);
            user.TeamId = userDTO.TeamId;
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }
    }
}
