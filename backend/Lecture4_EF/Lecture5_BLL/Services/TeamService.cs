﻿using AutoMapper;
using Lecture5_BLL.Exceptions;
using Lecture5_BLL.Services.Abstract;
using Lecture5_Common.DTO;
using Lecture5_Common.DTO.Team;
using Lecture5_DAL;
using Lecture5_DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_BLL.Services
{
    public sealed class TeamService : BaseService<Team>
    {
        public TeamService(AcademyDbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<TeamDTO> Create(TeamDTO teamDTO)
        {
            var team = _mapper.Map<Team>(teamDTO);
            _context.Teams.Add(team);
            await _context.SaveChangesAsync();
            return _mapper.Map<TeamDTO>(team);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var team = await _context.Teams.FindAsync(id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }
            var users = _context.Users.Where(u => u.TeamId == team.Id);
            foreach(var user in users)
            {
                user.TeamId = null;
                _context.Users.Update(user);
            }
            _context.Teams.Remove(team);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<TeamDTO>> Get()
        {
            var teams = await _context.Teams.ToListAsync();
            return _mapper.Map<ICollection<TeamDTO>>(teams);
        }

        public async Task<TeamDTO> Get(int id)
        {
            var team = await _context.Teams.FindAsync(id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }
            return _mapper.Map<TeamDTO>(team);
        }

        public async System.Threading.Tasks.Task Update(TeamDTO teamDTO)
        {
            var team = await _context.Teams.FindAsync(teamDTO.Id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), teamDTO.Id);
            }
            team.CreatedAt = teamDTO.CreatedAt;
            team.Name = teamDTO.Name;
            //team.Users = _mapper.Map<List<User>>(teamDTO.Users);
            _context.Teams.Update(team);
            await _context.SaveChangesAsync();
        }
    }
}
