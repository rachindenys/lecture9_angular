﻿using AutoMapper;
using Lecture5_BLL.Exceptions;
using Lecture5_Common.DTO;
using Lecture5_Common.DTO.Project;
using Lecture5_Common.DTO.Task;
using Lecture5_Common.DTO.Team;
using Lecture5_Common.DTO.User;
using Lecture5_DAL;
using Lecture5_DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_BLL.Services
{
    public sealed class LinqService
    {
        private readonly AcademyDbContext _context;
        private readonly IMapper _mapper;

        public LinqService
            (
                AcademyDbContext context,
                IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;
        }
        // 1
        public async Task<IDictionary<int, int>> CountTasksOfCurrentUser(int userId)
        {
            var query = await _context.Projects
                .Include(p => p.Tasks)
                .Where(p => p.AuthorId == userId)
                .ToDictionaryAsync(k => k.Id, v => v.Tasks.Count);
            if(query.Count == 0)
            {
                throw new NotFoundException(nameof(Project));
            }
            return query;
        }

        // 2

        public async Task<List<TaskDTO>> GetTasksOfCurrentUser(int userId)
        {
            var query = await _context.Tasks
                            .Where(t => t.PerformerId == userId && t.Name.Length < 45)
                            .ToListAsync();
            if(query.Count == 0)
            {
                throw new NotFoundException(nameof(Lecture5_DAL.Entities.Task));
            }
            return _mapper.Map<List<TaskDTO>>(query);
        }

        // 3

        public async Task<List<Tuple<int, string>>> GetFinishedThisYearTasksOfCurrentUser(int userId)
        {
            var query = await _context.Tasks
                        .Where(t => t.PerformerId == userId && t.FinishedAt.HasValue && t.FinishedAt.Value.Year == DateTime.Now.Year)
                        .Select((task) => new Tuple<int, string>(task.Id, task.Name))
                        .ToListAsync();
            if(query.Count == 0)
            {
                throw new NotFoundException(nameof(Lecture5_DAL.Entities.Task));
            }
            return query;
        }

        // 4

        public async Task<IEnumerable<TeamWithUsersOlder10DTO>> GetTeamsWithUsersOlderThanTen()
        {
            var query = await _context.Teams
                            .Include(t => t.Users)
                            .Select(t => new TeamWithUsersOlder10DTO
                            {
                                Id = t.Id,
                                TeamName = t.Name,
                                UsersOlder10 = _mapper.Map<IEnumerable<UserDTO>>(t.Users
                                                                .Where(u => DateTime.Now.Year - u.BirthDay.Year > 10)
                                                                .OrderByDescending(u => u.RegisteredAt)
                                                                .AsEnumerable())
                            })
                            .ToListAsync();
            var result = query.Where(t => t.UsersOlder10.Count() > 0);
            if (result.Count() == 0)
            {
                throw new NotFoundException(nameof(User));
            }
            return result;
        }

        // 5

        public async Task<IEnumerable<SortedUserByNameAndTasksDTO>> GetSortedUserByAscendingAndTasksByDescending()
        {
            var query = await _context.Users
                            .Include(u => u.Tasks)
                            .Select(u => new SortedUserByNameAndTasksDTO
                            {
                                UserName = u.FirstName,
                                SortedTasks = _mapper.Map<List<TaskDTO>>(u.Tasks
                                                                            .OrderByDescending(t => t.Name.Length)
                                                                            .ToList())
                            })
                            .ToListAsync()
;           var result = query.Where(u => u.SortedTasks.Count() > 0).OrderBy(u => u.UserName);
            if (result.Count() == 0)
            {
                throw new NotFoundException(nameof(User));
            }
            return result;
        }

        // 6

        public async Task<UserAnalyzeDTO> AnalyzeUserProjectsAndTasks(int userId)
        {
            var user = _mapper.Map<UserDTO>(await _context.Users.Where(u => u.Id == userId).FirstOrDefaultAsync());
            //var user = System.Threading.Tasks.Task.Run(async () => _mapper.Map<UserDTO>(await _context.Users.Where(u => u.Id == userId).FirstOrDefaultAsync()));
            var longestTask = _mapper.Map<TaskDTO>(_context.Tasks.ToList()
                .Where(t => t.PerformerId == userId && t.FinishedAt.HasValue)
                .OrderByDescending(t => t.FinishedAt - t.CreatedAt)
                .FirstOrDefault());
            /*var longestTask = System.Threading.Tasks.Task.Run(() => _mapper.Map<TaskDTO>(_context.Tasks.ToList()
                .Where(t => t.PerformerId == userId && t.FinishedAt.HasValue)
                .OrderByDescending(t => t.FinishedAt - t.CreatedAt)
                .FirstOrDefault()));*/
            var lastProject = _mapper.Map<ProjectDTO>(await _context.Projects.Where(p => p.AuthorId == userId).OrderByDescending(p => p.CreatedAt).FirstOrDefaultAsync());
            //var lastProject = System.Threading.Tasks.Task.Run(async () => _mapper.Map<ProjectDTO>(await _context.Projects.Where(p => p.AuthorId == userId).OrderByDescending(p => p.CreatedAt).FirstOrDefaultAsync()));
            //await System.Threading.Tasks.Task.WhenAll(user, longestTask, lastProject);
            if (user == null)
            {
                throw new NotFoundException(nameof(User));
            }
            var query = await _context.Users
                        .Include(u => u.Projects)
                            .ThenInclude(p => p.Tasks)
                        .Include(u => u.Tasks)
                        .Where(u => u.Id == userId)
                        .Select(u => new UserAnalyzeDTO
                        {
                            User = user,
                            LastProject = lastProject,
                            TotalTasksCount = u.Projects
                                            .OrderByDescending(p => p.CreatedAt)
                                            .FirstOrDefault().Tasks.Count,
                            TotalUncompletedAndCanceledTasks = u.Tasks
                                                                .Where(t => !t.FinishedAt.HasValue)
                                                                .Count(),
                            LongestTask = longestTask
                        }).FirstOrDefaultAsync();
            return query;
        }

        // 7

        public async Task<ProjectAnalyzeDTO> AnalyzeProjectTasksAndTeam(int projectId)
        {
            var query = await _context.Projects
                            .Include(p => p.Tasks)
                            .Include(p => p.Team)
                                .ThenInclude(t => t.Users)
                            .Where(p => p.Id == projectId)
                            .Select(p => new ProjectAnalyzeDTO
                            {
                                Project = _mapper.Map<ProjectDTO>(p),
                                LongestTaskByDescription = _mapper.Map<TaskDTO>(p.Tasks
                                                            .OrderByDescending(t => t.Description.Length)
                                                            .FirstOrDefault()),
                                ShortestTaskByName = _mapper.Map<TaskDTO>(p.Tasks
                                                        .Where(t => t.Name != null)
                                                        .OrderBy(t => t.Name.Length)
                                                        .FirstOrDefault()),
                                TotalTeamCount = p.Description.Length > 20 || p.Tasks.Count < 3 ? p.Team.Users.Count : 0
                            })
                            .FirstOrDefaultAsync();
            if(query.Project == null)
            {
                throw new NotFoundException(nameof(Project));
            }
            return query;
        }

        // 8
        public async Task<List<TaskDTO>> GetUncompletedTasks(int userId)
        {
            if (await _context.Users.FindAsync(userId) == null)
            {
                throw new NotFoundException(nameof(User));
            }
            var query = _mapper.Map<List<TaskDTO>>(await _context.Tasks
                        .Where(t => t.PerformerId == userId && t.State != TaskState.Finished)
                        .ToListAsync());
            return query;
        }
    }
}
