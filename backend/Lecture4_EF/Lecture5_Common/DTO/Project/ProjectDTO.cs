﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Lecture5_Common.DTO.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        //public UserDTO Author { get; set; }
        public int AuthorId { get; set; }
        //public TeamDTO Team { get; set; }
        public int TeamId { get; set; }
        //public List<TaskDTO> Tasks { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
