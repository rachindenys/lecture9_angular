﻿using Lecture5_Common.DTO.Task;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture5_Common.DTO.User
{
    public class SortedUserByNameAndTasksDTO
    {
        public string UserName { get; set; }
        public List<TaskDTO> SortedTasks { get; set; }
    }
}
