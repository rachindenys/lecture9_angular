﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Lecture5_Common.DTO.Task
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        //public ProjectDTO Project { get; set; }
        public int PerformerId { get; set; }
       // public UserDTO Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskStateDTO State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
