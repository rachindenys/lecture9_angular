﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lecture5_BLL.Services;
using Lecture5_Common.DTO;
using Lecture5_Common.DTO.Project;

namespace Lecture5_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectsController(
            ProjectService projectService
            )
        {
            _projectService = projectService;
        }

        [HttpPost]
        public async Task<ProjectDTO> Create([FromBody] ProjectDTO projectDTO)
        {
            var newProject = await _projectService.Create(projectDTO);
            if (newProject == null)
            {
                HttpContext.Response.StatusCode = 500;
                return null;
            }
            HttpContext.Response.StatusCode = 201;
            return newProject;
        }
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _projectService.Delete(id);
        }
        [HttpGet]
        public async Task<ICollection<ProjectDTO>> Get()
        {
            return await _projectService.Get();

        }
        [HttpGet("{id}")]
        public async Task<ProjectDTO> Get(int id)
        {
            return await _projectService.Get(id);
        }

        [HttpPut("{id}")]
        public async Task Update([FromBody] ProjectDTO projectDTO)
        {
            await _projectService.Update(projectDTO);
        }
    }
}
