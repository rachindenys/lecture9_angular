﻿using Lecture5_BLL.Services;
using Lecture5_Common.DTO;
using Lecture5_Common.DTO.User;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lecture5_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(
            UserService userService
            )
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<UserDTO> Create([FromBody] UserDTO userDTO)
        {
            var newUser = await _userService.Create(userDTO);
            if (newUser == null)
            {
                HttpContext.Response.StatusCode = 500;
                return null;
            }
            HttpContext.Response.StatusCode = 201;
            return newUser;
        }
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _userService.Delete(id);
            HttpContext.Response.StatusCode = 204;
        }
        [HttpGet]
        public async Task<ICollection<UserDTO>> Get()
        {
            return await _userService.Get();

        }
        [HttpGet("{id}")]
        public async Task<UserDTO> Get(int id)
        {
            return await _userService.Get(id);
        }

        [HttpPut("{id}")]
        public async Task Update([FromBody] UserDTO userDTO)
        {
            await _userService.Update(userDTO);
        }
    }
}
