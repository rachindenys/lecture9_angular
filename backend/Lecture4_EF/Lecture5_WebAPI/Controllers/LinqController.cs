﻿using Lecture5_BLL.Services;
using Lecture5_Common.DTO;
using Lecture5_Common.DTO.Task;
using Lecture5_Common.DTO.Team;
using Lecture5_Common.DTO.User;
using Lecture5_DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lecture5_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;

        public LinqController(
            LinqService linqService
            )
        {
            _linqService = linqService;
        }
        [HttpGet("1/{id}")]
        public async Task<IDictionary<int, int>> CountTasksOfCurrentUser(int id)
        {
            return await _linqService.CountTasksOfCurrentUser(id);
        }
        [HttpGet("2/{id}")]
        public async Task<List<TaskDTO>> GetTasksOfCurrentUser(int id)
        {
            return await _linqService.GetTasksOfCurrentUser(id);
        }
        [HttpGet("3/{id}")]
        public async Task<List<Tuple<int, string>>> GetFinishedThisYearTasksOfCurrentUser(int id)
        {
            return await _linqService.GetFinishedThisYearTasksOfCurrentUser(id);
        }
        [HttpGet("4")]
        public async Task<IEnumerable<TeamWithUsersOlder10DTO>> GetTeamsWithUsersOlderThanTen()
        {
            return await _linqService.GetTeamsWithUsersOlderThanTen();
        }
        [HttpGet("5")]
        public async Task<IEnumerable<SortedUserByNameAndTasksDTO>> GetSortedUserByAscendingAndTasksByDescending()
        {
            return await _linqService.GetSortedUserByAscendingAndTasksByDescending();
        }
        [HttpGet("6/{id}")]
        public async Task<UserAnalyzeDTO> AnalyzeUserProjectsAndTasks(int id)
        {
            return await _linqService.AnalyzeUserProjectsAndTasks(id);
        }
        [HttpGet("7/{id}")]
        public async Task<ProjectAnalyzeDTO> AnalyzeProjectTasksAndTeam(int id)
        {
            return await _linqService.AnalyzeProjectTasksAndTeam(id);
        }
        [HttpGet("8/{id}")]
        public async Task<List<TaskDTO>> GetUncompletedTasks(int id)
        {
            return await _linqService.GetUncompletedTasks(id);
        }
    }
}
