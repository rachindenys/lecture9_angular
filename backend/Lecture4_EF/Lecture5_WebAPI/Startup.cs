using AutoMapper;
using Lecture5_BLL.Services;
using Lecture5_Common.DTO;
using Lecture5_Common.DTO.Project;
using Lecture5_Common.DTO.Task;
using Lecture5_Common.DTO.Team;
using Lecture5_Common.DTO.User;
using Lecture5_DAL;
using Lecture5_DAL.Entities;
using Lecture5_WebAPI.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lecture5_WebAPI
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Lecture3_WebAPI", Version = "v1" });
            });

            services.AddScoped<ProjectService>();
            services.AddScoped<UserService>();
            services.AddScoped<TaskService>();
            services.AddScoped<TeamService>();
            services.AddScoped<LinqService>();
            var mapper = MapperConfiguration().CreateMapper();
            services.AddScoped(_ => mapper);

            services.AddDbContext<AcademyDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("AcademyDatabase")));
        }
        public MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Project, ProjectDTO>().ReverseMap();
                cfg.CreateMap<Lecture5_DAL.Entities.Task, TaskDTO>().ReverseMap();
                cfg.CreateMap<User, UserDTO>().ReverseMap();
                cfg.CreateMap<Team, TeamDTO>().ReverseMap();
                cfg.CreateMap<TaskState, TaskStateDTO>().ReverseMap();
            });
            return config;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Lecture3_WebAPI v1"));
            }
            app.UseCors(builder => builder
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials()
                .WithOrigins("http://localhost:4200"));
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
