import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectsListComponent } from './components/project/projects-list/projects-list.component';
import { ProjectDetailsComponent } from './components/project/project-details/project-details.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectFormComponent } from './components/project/project-form/project-form.component';
import { ProjectsPageComponent } from './components/project/projects-page/projects-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { TaskDetailsComponent } from './components/task/task-details/task-details.component';
import { TaskFormComponent } from './components/task/task-form/task-form.component';
import { TasksListComponent } from './components/task/tasks-list/tasks-list.component';
import { TasksPageComponent } from './components/task/tasks-page/tasks-page.component';
import { UserDetailsComponent } from './components/user/user-details/user-details.component';
import { UserFormComponent } from './components/user/user-form/user-form.component';
import { UsersListComponent } from './components/user/users-list/users-list.component';
import { UsersPageComponent } from './components/user/users-page/users-page.component';
import { TeamFormComponent } from './components/team/team-form/team-form.component';
import { TeamDetailsComponent } from './components/team/team-details/team-details.component';
import { TeamsListComponent } from './components/team/teams-list/teams-list.component';
import { TeamsPageComponent } from './components/team/teams-page/teams-page.component';
import { CustomDatePipe } from './pipes/custom-date.pipe';
import { registerLocaleData } from '@angular/common';
import localeUA from '@angular/common/locales/uk';
import { StateDirective } from './directives/state.directive';

registerLocaleData(localeUA, 'ua');

@NgModule({
  declarations: [
    AppComponent,
    ProjectsListComponent,
    ProjectDetailsComponent,
    ProjectFormComponent,
    ProjectsPageComponent,
    NavMenuComponent,
    TaskDetailsComponent,
    TaskFormComponent,
    TasksListComponent,
    TasksPageComponent,
    UserDetailsComponent,
    UserFormComponent,
    UsersListComponent,
    UsersPageComponent,
    TeamFormComponent,
    TeamDetailsComponent,
    TeamsListComponent,
    TeamsPageComponent,
    CustomDatePipe,
    StateDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
