import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/leave.guard';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css'],
  providers: [DatePipe]
})
export class UserFormComponent implements OnInit {
  public userForm!: FormGroup;
  @Input() user: User = {} as User;
  @Input() isCreate!: boolean;
  @Output() userChange = new EventEmitter<User | null>();
  public selectedUserId!: number;
  private unsubscribe$ = new Subject<void>();
  constructor(private datepipe: DatePipe,
              private userService: UserService) { }


  ngOnInit(): void {
    console.log("USER: " + this.user.firstName)
    let registeredAt_date = this.datepipe.transform(this.user.registeredAt, 'yyyy-MM-dd');
    let birthDay_date = this.datepipe.transform(this.user.birthDay, 'yyyy-MM-dd');
    this.userForm = new FormGroup({
    'firstName': new FormControl(this.user.firstName, [
        Validators.required,
        Validators.maxLength(15),
    ]),
    'lastName': new FormControl(this.user.lastName, [
      Validators.required,
      Validators.maxLength(15),
    ]),
    'email': new FormControl(this.user.email, [
      Validators.required,
      Validators.email,
      Validators.maxLength(25),
    ]),    
    'teamId': new FormControl(this.user.teamId, [
    ]), 
    'registeredAt': new FormControl(registeredAt_date, [
      Validators.required,
    ]),
    'birthDay': new FormControl(birthDay_date, [
      Validators.required,
    ])
    });
    
  }
  get firstName() { return this.userForm.get('firstName')!; }

  get lastName() { return this.userForm.get('lastName')!; }

  get email() { return this.userForm.get('email')!; }

  get teamId() { return this.userForm.get('teamId')!; }

  get registeredAt() { return this.userForm.get('registeredAt')!; }

  get birthDay() { return this.userForm.get('birthDay')!; }

  public onSubmit(): void{
    console.log('Emit event');
    let editedUser = this.userForm.value
    if(this.isCreate){
      editedUser.registeredAt = new Date(Date.now()).toJSON();
      console.log("Created: " + editedUser);
    }
    else{
      console.log("Edited " + editedUser);
    }
    this.userChange.emit(editedUser);
    this.userForm.reset();
  }

  public goBack(): void{
    console.log('Emit event');
    this.userChange.emit(null);
    this.user = {} as User;
    this.userForm.reset();
  }
}
