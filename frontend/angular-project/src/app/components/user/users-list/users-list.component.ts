import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  @Input() users: User[] = [];
  @Output() selectedUserId = new EventEmitter<number>();
  @Output() selectedUserToDeleteId = new EventEmitter<number>();
  private unsubscribe$ = new Subject<void>();
  public loadingUsers = false;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
  }

  public editUser(userId: number): void{
    console.log("List edit: " + userId);
    this.selectedUserId.emit(userId);
  }
  public deleteUser(userId: number): void{
    console.log("List delete: " + userId);
    this.selectedUserToDeleteId.emit(userId);
  }
}
