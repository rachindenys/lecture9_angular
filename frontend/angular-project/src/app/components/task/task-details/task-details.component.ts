import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit {
  @Input() public task!: Task;
  @Output() public taskToEditEvent = new EventEmitter<number>();
  @Output() public taskToDeleteEvent = new EventEmitter<number>();
  public taskState: string = "";

  constructor() { }

  ngOnInit(): void {
    switch(this.task.state){
      case 0: 
        this.taskState = "Created";
        break;
      case 1:
        this.taskState = "Started";
        break;
      case 2: 
        this.taskState = "Finished";
        break;
      case 3: 
        this.taskState = "Cancelled";
        break;
    }
  }

  editTask(){
    console.log("Details edit: " + this.task.id);
    this.taskToEditEvent.emit(this.task.id!);
  }

  deleteTask(){
    console.log("Details delete: " + this.task.id);
    this.taskToDeleteEvent.emit(this.task.id!);
  }
}
