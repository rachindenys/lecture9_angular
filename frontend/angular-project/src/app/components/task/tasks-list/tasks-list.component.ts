import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {

  @Input() tasks: Task[] = [];
  @Output() selectedTaskId = new EventEmitter<number>();
  @Output() selectedTaskToDeleteId = new EventEmitter<number>();
  private unsubscribe$ = new Subject<void>();
  public loadingTasks = false;

  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit(): void {
  }

  public editTask(taskId: number): void{
    console.log("List edit: " + taskId);
    this.selectedTaskId.emit(taskId);
  }
  public deleteTask(taskId: number): void{
    console.log("List delete: " + taskId);
    this.selectedTaskToDeleteId.emit(taskId);
  }
}
