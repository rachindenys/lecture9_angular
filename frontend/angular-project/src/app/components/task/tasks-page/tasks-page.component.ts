import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-tasks-page',
  templateUrl: './tasks-page.component.html',
  styleUrls: ['./tasks-page.component.css']
})
export class TasksPageComponent implements OnInit {
  public hideTaskCreate: boolean = true;
  public isCreate: boolean = false;
  public tasks: Task[] = [];
  public selectedTask!: Task;
  public selectedTaskIndex: number = -1;
  public loadingTasks: boolean = false;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private taskService: TaskService
  ) { }
  title = "Tasks";
  ngOnInit(): void {
    this.getTasks();
    console.log(this.tasks);
  }

  public getTasks(): void {
    this.loadingTasks = true;
    this.taskService
        .getTasks()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
            (resp) => {
                this.loadingTasks = false;
                this.tasks = resp.body!
                console.log(this.tasks);
            },
            (error) => (this.loadingTasks = false)
        );
  }

  public editTask(taskId: number){
    this.loadingTasks = true;
    if(this.hideTaskCreate){
      this.hideTaskCreate = false;
      this.selectedTaskIndex = this.tasks.findIndex((t) => t.id === taskId);
      this.selectedTask = this.tasks[this.tasks.findIndex((t) => t.id === taskId)];
    }
    else{
      this.hideTaskCreate = true;
      this.hideTaskCreate = false;
      this.selectedTaskIndex = this.tasks.findIndex((t) => t.id === taskId);
      this.selectedTask = this.tasks[this.tasks.findIndex((t) => t.id === taskId)];
    }
    this.loadingTasks = false;
  }

  public deleteTask(taskId: number){
    console.log("Page delete: " + taskId);
    this.loadingTasks = true;
    this.taskService.deleteTask(taskId).subscribe(x => {
      console.log("Deleted " + x);
      this.tasks.splice(this.tasks.findIndex((t) => t.id === taskId), 1);
    });
    this.loadingTasks = false;
  }

  showCreate() {
    this.hideTaskCreate = false;
    this.selectedTask = {} as Task;
    this.selectedTaskIndex = -1;
    this.isCreate = true;
  }

  saveTask(newTask: Task | null): void{
    this.loadingTasks = true;
    if(newTask){
    if(this.selectedTaskIndex === -1){
        newTask.createdAt = new Date(Date.now());
        this.taskService.createTask(newTask).pipe(takeUntil(this.unsubscribe$)).subscribe(x => {
          console.log("Created " + x.body?.id);
          newTask.id = x.body?.id;
          this.tasks.push(newTask);
        });
    } else {
        newTask.id = this.tasks[this.selectedTaskIndex].id;
        
        this.taskService.updateTask(this.tasks[this.selectedTaskIndex].id!, newTask).pipe(takeUntil(this.unsubscribe$)).subscribe(x => {
          console.log("Edited " + x.body);
        });
        this.tasks[this.selectedTaskIndex] = newTask;
      }
    }
    this.selectedTaskIndex = -1;
    this.hideTaskCreate = true;
    this.isCreate = false;
    this.loadingTasks = false;
}

  select(index: number) {
    let task = this.tasks[index];
    this.selectedTask = {
        projectId: task.projectId,
        performerId: task.performerId,
        name: task.name,
        description: task.description,
        state: task.state,
        createdAt: task.createdAt,
        finishedAt: task.finishedAt
     } as Task;
    this.selectedTaskIndex = index;
    this.hideTaskCreate = false;
    console.log(this.selectedTask)
  }
  canDeactivate() : boolean | Observable<boolean>{
     
    if(!this.hideTaskCreate){
        return confirm("You have unsaved changes. Are you sure you want to exit?");
    }
    else{
        return true;
    }
  }
}
