import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css'],
})
export class ProjectsListComponent implements OnInit {
  
  @Input() projects: Project[] = [];
  @Output() selectedProjectId = new EventEmitter<number>();
  @Output() selectedProjectToDeleteId = new EventEmitter<number>();
  private unsubscribe$ = new Subject<void>();
  public loadingProjects = false;

  constructor(
    private projectService: ProjectService
  ) { }

  ngOnInit(): void {
  }

  public editProject(projectId: number): void{
    console.log("List edit: " + projectId);
    this.selectedProjectId.emit(projectId);
  }
  public deleteProject(projectId: number): void{
    console.log("List delete: " + projectId);
    this.selectedProjectToDeleteId.emit(projectId);
  }
}
