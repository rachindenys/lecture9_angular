import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Project } from 'src/app/models/project';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {
  @Input() public project!: Project;
  @Output() public projectToEditEvent = new EventEmitter<number>();
  @Output() public projectToDeleteEvent = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {

  }

  editProject(){
    console.log("Details edit: " + this.project.id);
    this.projectToEditEvent.emit(this.project.id!);
  }

  deleteProject(){
    console.log("Details delete: " + this.project.id);
    this.projectToDeleteEvent.emit(this.project.id!);
  }
}
