import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { empty } from 'rxjs/internal/observable/empty';
import { catchError } from 'rxjs/internal/operators/catchError';
import { map } from 'rxjs/internal/operators/map';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-projects-page',
  templateUrl: './projects-page.component.html',
  styleUrls: ['./projects-page.component.css']
})
export class ProjectsPageComponent implements OnInit {
  public hideProjectCreate: boolean = true;
  public isCreate: boolean = false;
  public projects: Project[] = [];
  public selectedProject!: Project;
  public selectedProjectIndex: number = -1;
  public loadingProjects: boolean = false;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private projectService: ProjectService
  ) { }
  title = "Projects";
  ngOnInit(): void {
    this.getProjects();
    console.log(this.projects);
  }

  public getProjects(): void {
    this.loadingProjects = true;
    this.projectService
        .getProjects()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
            (resp) => {
                this.loadingProjects = false;
                this.projects = resp.body!
                console.log(this.projects);
            },
            (error) => (this.loadingProjects = false)
        );
  }

  public editProject(projectId: number){
    this.loadingProjects = true;
    if(this.hideProjectCreate){
      this.hideProjectCreate = false;
      this.selectedProjectIndex = this.projects.findIndex((pr) => pr.id === projectId);
      this.selectedProject = this.projects[this.projects.findIndex((pr) => pr.id === projectId)];
    }
    else{
      this.hideProjectCreate = true;
      this.hideProjectCreate = false;
      this.selectedProjectIndex = this.projects.findIndex((pr) => pr.id === projectId);
      this.selectedProject = this.projects[this.projects.findIndex((pr) => pr.id === projectId)];
    }
    this.loadingProjects = false;
  }

  public deleteProject(projectId: number){
    console.log("Page delete: " + projectId);
    this.loadingProjects = true;
    this.projectService.deleteProject(projectId).subscribe(x => {
      console.log("Deleted " + x);
      this.projects.splice(this.projects.findIndex((pr) => pr.id === projectId), 1);
    });
    this.loadingProjects = false;
  }

  showCreate() {
    this.hideProjectCreate = false;
    this.selectedProject = {} as Project;
    this.selectedProjectIndex = -1;
    this.isCreate = true;
  }

  saveProject(newProject: Project | null): void{
    this.loadingProjects = true;
    if(newProject){
    if(this.selectedProjectIndex === -1){
        newProject.createdAt = new Date(Date.now());
        this.projectService.createProject(newProject).pipe(takeUntil(this.unsubscribe$)).subscribe(x => {
          console.log("Created " + x.body?.id);
          newProject.id = x.body?.id;
          this.projects.push(newProject);
        });
    } else {
        newProject.id = this.projects[this.selectedProjectIndex].id;
        
        this.projectService.updateProject(this.projects[this.selectedProjectIndex].id!, newProject).pipe(takeUntil(this.unsubscribe$)).subscribe(x => {
          console.log("Edited " + x.body);
        });
        this.projects[this.selectedProjectIndex] = newProject;
      }
    }
    this.selectedProjectIndex = -1;
    this.hideProjectCreate = true;
    this.isCreate = false;
    this.loadingProjects = false;
}

  select(index: number) {
    let project = this.projects[index];
    this.selectedProject = {
        name: project.name,
        description: project.description,
        deadline: project.deadline,
        authorId: project.authorId,
        teamId: project.teamId
     } as Project;
    this.selectedProjectIndex = index;
    this.hideProjectCreate = false;
    console.log(this.selectedProject)
  }
  canDeactivate() : boolean | Observable<boolean>{
     
    if(!this.hideProjectCreate){
        return confirm("You have unsaved changes. Are you sure you want to exit?");
    }
    else{
        return true;
    }
  }
}
