import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-team-form',
  templateUrl: './team-form.component.html',
  styleUrls: ['./team-form.component.css'],
  providers: [DatePipe]
})
export class TeamFormComponent implements OnInit {
  public teamForm!: FormGroup;
  @Input() team: Team = {} as Team;
  @Input() isCreate!: boolean;
  @Output() teamChange = new EventEmitter<Team | null>();
  public selectedTeamId!: number;
  private unsubscribe$ = new Subject<void>();
  constructor(private datepipe: DatePipe,
              private teamService: TeamService) { }


  ngOnInit(): void {
    console.log("TEAM: " + this.team.name)
    let createdAt_date = this.datepipe.transform(this.team.createdAt, 'yyyy-MM-dd');
    this.teamForm = new FormGroup({
      'name': new FormControl(this.team.name, [
        Validators.required,
      ]),
      'createdAt': new FormControl(createdAt_date, [
        Validators.required,
      ]),
    });
    
  }
  get name() { return this.teamForm.get('name')!; }

  get createdAt() { return this.teamForm.get('createdAt')!; }

  public onSubmit(): void{
    console.log('Emit event');
    let editedTeam = this.teamForm.value
    if(this.isCreate){
      editedTeam.createdAt = new Date(Date.now()).toJSON();
      console.log("Created: " + editedTeam);
    }
    else{
      console.log("Edited " + editedTeam);
    }
    this.teamChange.emit(editedTeam);
    this.teamForm.reset();
  }

  public goBack(): void{
    console.log('Emit event');
    this.teamChange.emit(null);
    this.team = {} as Team;
    this.teamForm.reset();
  }
}
