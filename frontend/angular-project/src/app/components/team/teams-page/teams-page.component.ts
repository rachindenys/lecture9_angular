import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { takeUntil } from 'rxjs/operators';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-teams-page',
  templateUrl: './teams-page.component.html',
  styleUrls: ['./teams-page.component.css']
})
export class TeamsPageComponent implements OnInit {
  public hideTeamCreate: boolean = true;
  public isCreate: boolean = false;
  public teams: Team[] = [];
  public selectedTeam!: Team;
  public selectedTeamIndex: number = -1;
  public loadingTeams: boolean = false;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private teamService: TeamService
  ) { }
  title = "Teams";
  ngOnInit(): void {
    this.getTeams();
    console.log(this.teams);
  }

  public getTeams(): void {
    this.loadingTeams = true;
    this.teamService
        .getTeams()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
            (resp) => {
                this.loadingTeams = false;
                this.teams = resp.body!
                console.log(this.teams);
            },
            (error) => (this.loadingTeams = false)
        );
  }

  public editTeam(teamId: number){
    this.loadingTeams = true;
    if(this.hideTeamCreate){
      this.hideTeamCreate = false;
      this.selectedTeamIndex = this.teams.findIndex((pr) => pr.id === teamId);
      this.selectedTeam = this.teams[this.teams.findIndex((pr) => pr.id === teamId)];
    }
    else{
      this.hideTeamCreate = true;
      this.hideTeamCreate = false;
      this.selectedTeamIndex = this.teams.findIndex((pr) => pr.id === teamId);
      this.selectedTeam = this.teams[this.teams.findIndex((pr) => pr.id === teamId)];
    }
    this.loadingTeams = false;
  }

  public deleteTeam(teamId: number){
    console.log("Page delete: " + teamId);
    this.loadingTeams = true;
    this.teamService.deleteTeam(teamId).subscribe(x => {
      console.log("Deleted " + x);
      this.teams.splice(this.teams.findIndex((pr) => pr.id === teamId), 1);
    });
    this.loadingTeams = false;
  }

  showCreate() {
    this.hideTeamCreate = false;
    this.selectedTeam = {} as Team;
    this.selectedTeamIndex = -1;
    this.isCreate = true;
  }

  saveTeam(newTeam: Team | null): void{
    this.loadingTeams = true;
    if(newTeam){
    if(this.selectedTeamIndex === -1){
        newTeam.createdAt = new Date(Date.now());
        this.teamService.createTeam(newTeam).pipe(takeUntil(this.unsubscribe$)).subscribe(x => {
          console.log("Created " + x.body?.id);
          newTeam.id = x.body?.id;
          this.teams.push(newTeam);
        });
    } else {
        newTeam.id = this.teams[this.selectedTeamIndex].id;
        
        this.teamService.updateTeam(this.teams[this.selectedTeamIndex].id!, newTeam).pipe(takeUntil(this.unsubscribe$)).subscribe(x => {
          console.log("Edited " + x.body);
        });
        this.teams[this.selectedTeamIndex] = newTeam;
      }
    }
    this.selectedTeamIndex = -1;
    this.hideTeamCreate = true;
    this.isCreate = false;
    this.loadingTeams = false;
}

  select(index: number) {
    let team = this.teams[index];
    this.selectedTeam = {
      name: team.name,
      createdAt: team.createdAt
     } as Team;
    this.selectedTeamIndex = index;
    this.hideTeamCreate = false;
    console.log(this.selectedTeam)
  }
  canDeactivate() : boolean | Observable<boolean>{
     
    if(!this.hideTeamCreate){
        return confirm("You have unsaved changes. Are you sure you want to exit?");
    }
    else{
        return true;
    }
  }
}
