import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from '../models/project';
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  public routePrefix = '/api/projects';

  constructor(private httpService: HttpInternalService) { }

  public getProjects(): Observable<HttpResponse<Project[]>>{
    return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
  }
  public createProject(project: Project): Observable<HttpResponse<Project>>{
    return this.httpService.postFullRequest<Project>(`${this.routePrefix}`, project);
  }

  public updateProject(id: number, project: Project): Observable<HttpResponse<Project>>{
      return this.httpService.putFullRequest<Project>(`${this.routePrefix}/${id}`, project);
  }
  public deleteProject(id: number): Observable<HttpResponse<Project>> {
      return this.httpService.deleteFullRequest<Project>(`${this.routePrefix}/${id}`);
  }
}
