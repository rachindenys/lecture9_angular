import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
@Pipe({
  name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {
  transform(value: Date): any {
    let datePipe = new DatePipe('ua');
    
    return datePipe.transform(value, 'longDate')?.slice(0, -3);
  }

}
