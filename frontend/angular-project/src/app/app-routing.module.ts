import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectFormComponent } from './components/project/project-form/project-form.component';
import { ProjectsPageComponent } from './components/project/projects-page/projects-page.component';
import { TasksPageComponent } from './components/task/tasks-page/tasks-page.component';
import { TeamsPageComponent } from './components/team/teams-page/teams-page.component';
import { UserFormComponent } from './components/user/user-form/user-form.component';
import { UsersPageComponent } from './components/user/users-page/users-page.component';
import { LeaveGuard } from './guards/leave.guard';

const routes: Routes = [
  { path: "projects", component: ProjectsPageComponent, canDeactivate: [LeaveGuard]},
  { path: "tasks", component: TasksPageComponent, canDeactivate: [LeaveGuard]},
  { path: "teams", component: TeamsPageComponent, canDeactivate: [LeaveGuard]},
  { path: "users", component: UsersPageComponent, canDeactivate: [LeaveGuard]},
  { path: "user/:id", component: UserFormComponent, canDeactivate: [LeaveGuard]}
];


//{ path: "teams", component: TeamsPageComponent, canDeactivate: [CanDeactivateGuard]},
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [LeaveGuard]
})
export class AppRoutingModule { }
